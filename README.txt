***********
* README: *
***********

DESCRIPTION:
------------
This module provides an email field type.


INSTALLATION:
-------------
Note install geshi code highlighter - http://qbnz.com/highlighter/ 

1. Place the entire email directory into your Drupal sites/all/modules/
   directory.

2. Enable the email module by navigating to:

     administer > modules

3. Add downloaded geshi files into module directory on the same heighrarchy (.info)

4. Enable codeformatter field from cck->add new fields

5. put required parameter after selecting code formatter

6. node -> add content you will see field with textarea codeformaater label

7. Put you code to show on node/ and select language for code

8. On front it will show formatted code.


Features:
---------
  * validation of emails
  * turns addresses into mailto links
  * encryption of email addresses with
      o Invisimail (Drupal 5 + 6) (module needs to be installed)
      o SpamSpan (Drupal 6 only) (module needs to be installed)
  * contact form (see Display settings)
  * provides Tokens
  * exposes fields to Views


Note:
-----
To enable encryption of contact form, see settings under the Display fields tabs 


Author:
-------
Anshul Udaure
anshul.udapure@gmail.com